@javascript

Feature: HomeController
  Scenario: Insert new item
    Given I go to "/home"
    Then I fill in "insert-name" with "Green"
    And I click "btn-insert"
    And I wait for 4 seconds

  Scenario: Export items
    Given I go to "/home"
    Then I fill in "insert-name" with "Green"
    And I click "btn-insert"
    Then I fill in "insert-name" with "Blue"
    And I click "btn-insert"
    Then I fill in "insert-name" with "Green"
    And I click "btn-insert"
    Then I click "export"
    And I wait for 6 seconds
