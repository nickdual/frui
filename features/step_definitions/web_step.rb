When /^I wait for (\d+) seconds?$/ do |secs|
  sleep secs.to_i
end

When /^(?:|I )go to "([^"]*)"$/ do |page_name|
  visit page_name
end

When /^I press button "([^"]*)"$/ do |button|
  click_button(button)

end

When /^(?:|I )press "([^"]*)"$/ do |button|
  click_link(button)

end
When /^(?:|I )click "([^"]*)"$/ do |button|
  click_on(button)

end
When /^(?:|I )follow "([^"]*)"$/ do |link|
  click_link(link)
end

When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  fill_in(field, :with => value)
end

When /^(?:|I )fill in "([^"]*)" for "([^"]*)"$/ do |value, field|
  fill_in(field, :with => value)
end