require 'spec_helper'

describe HomeController do
  before(:each) do
    @item1 = FactoryGirl.create(:item1)
    @item2 = FactoryGirl.create(:item2)
    @item3 = FactoryGirl.create(:item3)
    @item4 = FactoryGirl.create(:item4)
    @item5 = FactoryGirl.create(:item5)
  end
  describe "get index" do
    it "get right value" do
      get :index,:format => :json
      @items = ActiveSupport::JSON.decode(response.body)
      @items.length.should == 4 &&
          @items[0]["name"] == @item3["name"]
    end
    it "renders the index template" do
      get :index
      response.should render_template("index")
    end
    it "has a 200 status code" do
      get :index
      expect(response.code).to eq("200")
    end
  end
  describe "post insert" do
    it "key number one" do
      request.env["HTTP_ACCEPT"] = "application/json"
      post :insert, {:name => "Blue"}
      @items = ActiveSupport::JSON.decode(response.body)["data"]
      @items.length.should == 4 &&
          @items[0]["name"] == @item3["name"]
    end
    it "increase rank" do
      request.env["HTTP_ACCEPT"] = "application/json"
      post :insert, {:name => "Green"}
      post :insert, {:name => "Green"}
      @items = ActiveSupport::JSON.decode(response.body)["data"]
      @items.length.should == 4 &&
          @items[2]["name"] == "Green"
    end
    it "new item" do
      request.env["HTTP_ACCEPT"] = "application/json"
      post :insert, {:name => "Silver"}
      @items = ActiveSupport::JSON.decode(response.body)["data"]
      @items.length.should == 4 &&
          @items[3]["name"] != "Silver"
    end
  end
end