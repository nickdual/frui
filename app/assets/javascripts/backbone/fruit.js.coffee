#= require_self

#= require_tree ./templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers

window.Fruit =
  Models: {}
  Collections: {}
  Views: {}
  Routers: {}
  initialize: ->
    new Fruit.Views.HomeIndex()

$(document).ready ->
  Fruit.initialize()
