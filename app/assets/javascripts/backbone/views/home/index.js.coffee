class Fruit.Views.HomeIndex extends Backbone.View
  el: 'body'
  events:
    'click #insert > button': 'insertEvent'
    'keypress #insert > input': 'insertEvent'
  initialize: ->

  insertEvent: (evt) ->
    return if evt.type == 'keypress' && evt.which != 13
    $.ajax({
      type: 'POST'
      url: '/home/insert'
      async: true
      jsonpCallback: 'jsonCallback'
      dataType: 'json'
      data: {name: $(evt.currentTarget).parent().children('input').val()},
      beforeSend: () ->
      success: (response) ->
        if response.status == 'success'
          tag_tbody = $('#list-items > tbody')
          _.each(response.data, (value, index) ->
            tag_tr = tag_tbody.children('tr:nth-child('+(index+1)+')')
            if tag_tr.length == 0
              tag_tbody.append('<tr id="itemid-'+value.id+'">
                                  <td class="center">
                                    '+value.id+'
                                  </td>
                                  <td class="wrap-content center">
                                    '+value.name+'
                                  </td>
                                  <td class="wrap-content center">
                                      '+value.number+'
                                  </td>
                                  <td class="wrap-content center">
                                      '+value.created_at+'
                                  </td>
                                </tr>')
            else if +tag_tr.attr('id').substr(7) == value.id
              tag_tr.children('td:nth-child(3)').text(value.number)
            else
              tag_tr.attr('id', 'itemid-'+value.id)
              tag_tr.children('td:nth-child(1)').text(value.id)
              tag_tr.children('td:nth-child(2)').text(value.name)
              tag_tr.children('td:nth-child(3)').text(value.number)
              tag_tr.children('td:nth-child(4)').text(value.created_at)
          )
    });