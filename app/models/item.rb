class Item < ActiveRecord::Base
  attr_accessible :name, :number

  def self.to_csv(items, columns, options = {})
    column_name = []
    columns.each do |column|
      column_name << column_names[column]
    end
    CSV.generate(options) do |csv|
      csv << column_name
      items.each do |item|
        csv << item.attributes.values_at(*column_name)
      end
    end
  end

end
